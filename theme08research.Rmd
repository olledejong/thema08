---
title: "Model of Spiking Neurons"
author: "Menno Gerbens & Olle de Jong"
date: "4-6-2018"
output:
  pdf_document:
    toc: true
    toc_depth: 3
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

\pagebreak

## Introductie
Een neuron is een type cel die zijn werk doet in het zenuwstelsel. Deze cellen kunnen informatie ontvangen, verwerken en doorgeven.
Dit alles kunnen deze cellen realiseren zonder singnaalsterkte te verliezen. Een neuron heeft een cellichaam (soma), en aan beide kanten
heeft dit cellichaam uitlopers (Fig 1). Deze uitlopers worden geclassificeerd als axonen en dendrieten. Axonen geleiden van de neuron af, en dendrieten er meestal naar toe.
Een zenuwcel is zoals de meeste cellen omringd door een celmembraan die het einde van de cel aanduid. Hier binnen bevindt zich cytoplasma met daarin cytosol en ook 
organellen met een eigen membraan zoals; het golgi, lysosomen, mitochondria en het endoplasmatisch reticulum. 
Signalen tussen neuronen wordt over gebracht door middel van synapsen. Dit signaal wordt overgebracht door een neurotransmitters.
Meestal wordt zo'n signaal geproduceerd aan het uiteinde van een axon en meestal wordt het overgebracht op een dendriet van een
andere neuron, maar ook komt het voor dat het signaal van axon naar cellichaam of van dendriet naar dendriet.  [4] [5]
\newline
\newline Om te kunnen begrijpen hoe ons brein werkt is het belangrijk dat we in kaart kunnen brengen welke 
soorten neuronen zich waar bevinden en hoe ze zich gedragen. Om enigzins zichtbaar te maken hoe het gedrag 
van de verschillende typen neuronen eruit ziet wordt een model gemaakt. Het model dat in dit artikel wordt
gebruikt is afkomstig uit het artikel "Simple Model of Spiking Neurons" door Eugene M. Izhikevich [3]. 
Dit model reproduceert "spiking" en "bursting" van al bekende typen cortical neuronen.
![Visuele ondersteuning voor het model. Neuron en een neuron spike](drawing_neuron.png)
Figuur 1: Visuele ondersteuning voor het model. Neuron en een neuron spike.

\pagebreak

## Materiaal en Methoden
Om het model van "spiking neurons" te realiseren is het in RStudio [1] ingebouwde pakket deSolve [2] gebruikt. 
deSolve is een pakket met functies die initiele waarde problemen van eerste orde ordinary differential equations ('ODE'), 
partial differential equations ('PDE') en differential algebraic equations ('DAE') kan oplossen.

### Beschrijving van het model
Het gebruikte en gerepliceerde model [3] is gebaseerd op de volgende twee differentiaalvergelijkingen, 

$$ v' = 0.04v^2 + 5v + 140 -u + I $$
$$ u' = a(b * v - u) $$
en een extra "after-spike reset" achteraf.
als v gelijk of groter is dan 30 mV dan worden de volgende twee vergelijkingen gebruikt:
$$ v = c $$
$$ u = u + d $$

Variabele v representeert het membraan potentiaal van de neuron, u representeert een membraan herstel variabele. 
Kalium ionenstromen worden geactiveerd en Natrium ionenstromen worden geinactiveerd en dit veroorzaakt negatieve feedback die wordt doorgegeven aan v. \newline
Wanneer een spike het hoogtepunt bereikt, 30 mV of meer, zal door middel van de after-spike reset u en v worden gereset. \newline
I geeft synaptische stromen door aan de formule. \newline
Zoals veel neuronen heeft dit model geen vaste threshold, deze hangt af van de voorafgaande waarde van het membraan potentiaal voorafgaand aan de spike. \newline
\newline
- De parameter a geeft de snelheid van de herstel variabele van u weer. Hoe lager de waarde, hoe langzamer het herstel. \newline
- De parameter b geeft de gevoeligheid van de herstel variabele u aan in relatie tot de subthreshold fluctiaties van het membraan potentiaal v. Normale typische waarde: b = 0.2 \newline
- De parameter c geeft de after-spike reset waarde van het membraan potentiaal v weer. Normale typische waarde: c = -65mV \newline
- De parameter d geeft de after-spike reset waarde van de herstel variabele u. Normale typische waarde d = 2

\pagebreak

## Resultaten

Met het onderstaande model wordt "neuron spiking" gemodelleerd. 
Het model bestaat uit twee formules met 5 parameters(a,b,c,d en I). Voor de rest bestaat het model uit twee intiële waardes(v en u). 
Door het veranderen van de parameters kunnen gevarieerde soorten spike grafieken gemodelleerd worden die verbonden zijn aan verschillende soorten neuronen op verschillende plekken in de hersens.

```{r bs}
#import library
library(deSolve)

#define parameters
parameters_st <- c(a = 0.02, b = 0.2, c = -50,
                  d =2, I = 10)
#intial values
state <- c(v = -60,
           u = -10)

#time indication
times <- seq(0, 300, by = 1)

#spiking function
spiking <-function(t,y,parms){
    with(as.list(c(y,parms)),{
      #formula's
      dv <- (0.04*(v^2))+(5*v)+140-u+I
      du <- a*((b*v)-u)
        return(list(c(dv,du)))
    }
    )
}
#root function
root <- function(times,state,parms){
  #if v is above 30 root = False
  return(state[1]+30)
}
# if statment if v is above 30
event <- function(time,state,parms){ 
  with(as.list(c(state, parms)), { 
    #change intial values
    state[1] <- c 
    state[2] <- state[2] + d 
    return(state) 
  }) 
} 

#calculate deSolve graph matrix
out_steady <- lsode(times = times, y = state,
                  parms = parameters_st, 
                  func = spiking, 
                  events = list(func = event, 
                                root = TRUE),
                  rootfun = root
                  )
#plot deSolve results
plot(out_steady[,1],out_steady[,2], 
     type = "l",
     main = "Spiking membrane potential of neurons",
     xlab = "time(ms)",
     ylab = "membrane potential(mV)")
lines(out_steady[,3], col = "red")
```

### Regular Spiking Neurons
```{r model_RS}
library(deSolve)
#define parameters
parameters_st <- c(a = 0.02, b = 0.2, c = -65, 
                   d = 8, I = 10)

#intial values
state <- c(v = -60,
           u = -30)

#time indication
times <- seq(0, 300, by = 1)

#spiking function
spiking <-function(t,y,parms){
    with(as.list(c(y,parms)),{
      dv <- (0.04*(v^2))+(5*v)+140-u+I
      du <- a*((b*v)-u)
        return(list(c(dv,du)))
    }
    )
}

root <- function(times,state,parms){
  #if v is above 30 root = False
  return(state[1]+30)
}
# if statment if v is above 30
event <- function(time,state,parms){ 
  with(as.list(c(state, parms)), { 
    #change intial values
    state[1] <- c 
    state[2] <- state[2] + d 
    return(state) 
  }) 
} 

#calculate deSolve graph matrix
out_steady <- lsode(times = times, y = state,
                  parms = parameters_st, 
                  func = spiking, 
                  events = list(func = event, 
                                root = TRUE),
                  rootfun = root
                  )

#plot regular spiking (RS)
plot(out_steady[,1],out_steady[,2], 
     type = "l",
     main = "Spiking membrane potential of neurons (regular spiking)",
     xlab = "time(ms)",
     ylab = "membrane potential(mV)")
lines(out_steady[,3], col = "red")
```
\newline
Figuur 2: "Regular spiking" neuronen zijn het meest typisch in de cortex. 

\newpage
### Intrinsically Bursting Neurons
```{r model_IB}
library(deSolve)
#define parameters
parameters_st <- c(a = 0.02, b = 0.2, c = -55, 
                   d = 4, I = 10)

#intial values
state <- c(v = -60,
           u = -30)

#time indication
times <- seq(0, 300, by = 1)

#spiking function
spiking <-function(t,y,parms){
    with(as.list(c(y,parms)),{
      dv <- (0.04*(v^2))+(5*v)+140-u+I
      du <- a*((b*v)-u)
        return(list(c(dv,du)))
    }
    )
}

root <- function(times,state,parms){
  #if v is above 30 root = False
  return(state[1]+30)
}
# if statment if v is above 30
event <- function(time,state,parms){ 
  with(as.list(c(state, parms)), { 
    #change intial values
    state[1] <- c 
    state[2] <- state[2] + d 
    return(state) 
  }) 
} 

#calculate deSolve graph matrix
out_steady <- lsode(times = times, y = state,
                  parms = parameters_st, 
                  func = spiking, 
                  events = list(func = event, 
                                root = TRUE),
                  rootfun = root
                  )

#plot intrinsically bursting (IB)
plot(out_steady[,1],out_steady[,2], 
     type = "l",
     main = "Spiking membrane potential of neurons (intrinsically bursting)",
     xlab = "time(ms)",
     ylab = "membrane potential(mV)")
lines(out_steady[,3], col = "red")
```
\newline 
Figuur 3: "Intrinsically bursting" neuronen zijn neuronen die in het begin een typische "burst" afgeven waarna er normale "single spikes" volgen.

\newpage
### Chattering Neurons
```{r model_CH}
library(deSolve)
#define parameters
parameters_st <- c(a = 0.02, b = 0.2, c = -55, 
                   d = 2, I = 10)

#intial values
state <- c(v = -60,
           u = -20)

#time indication
times <- seq(0, 300, by = 1)

#spiking function
spiking <-function(t,y,parms){
    with(as.list(c(y,parms)),{
      dv <- (0.04*(v^2))+(5*v)+140-u+I
      du <- a*((b*v)-u)
        return(list(c(dv,du)))
    }
    )
}

root <- function(times,state,parms){
  #if v is above 30 root = False
  return(state[1]+30)
}
# if statment if v is above 30
event <- function(time,state,parms){ 
  with(as.list(c(state, parms)), { 
    #change intial values
    state[1] <- c 
    state[2] <- state[2] + d 
    return(state) 
  }) 
} 

#calculate deSolve graph matrix
out_steady <- lsode(times = times, y = state,
                  parms = parameters_st, 
                  func = spiking, 
                  events = list(func = event, 
                                root = TRUE),
                  rootfun = root
                  )

#plot chattering neurons (CH)
plot(out_steady[,1],out_steady[,2], 
     type = "l",
     main = "Spiking membrane potential of neurons (chattering)",
     xlab = "time(ms)",
     ylab = "membrane potential(mV)")
lines(out_steady[,3], col = "red")
```
\newline 
Figuur 4: "Chattering" neuronen zijn neuronen die stereotypische "bursts" afgeven van spikes die zich dicht bij elkaar bevinden.

\newpage
### Fast Spiking Neurons
```{r model_FS}
library(deSolve)
#define parameters
parameters_st <- c(a = 0.1, b = 0.2, c = -65, 
                   d = 2, I = 10)

#intial values
state <- c(v = -60,
           u = -20)

#time indication
times <- seq(0, 300, by = 1)

#spiking function
spiking <-function(t,y,parms){
    with(as.list(c(y,parms)),{
      dv <- (0.04*(v^2))+(5*v)+140-u+I
      du <- a*((b*v)-u)
        return(list(c(dv,du)))
    }
    )
}

root <- function(times,state,parms){
  #if v is above 30 root = False
  return(state[1]+30)
}
# if statment if v is above 30
event <- function(time,state,parms){ 
  with(as.list(c(state, parms)), { 
    #change intial values
    state[1] <- c 
    state[2] <- state[2] + d 
    return(state) 
  }) 
} 

#calculate deSolve graph matrix
out_steady <- lsode(times = times, y = state,
                  parms = parameters_st, 
                  func = spiking, 
                  events = list(func = event, 
                                root = TRUE),
                  rootfun = root
                  )

#plot fast spiking (FS)
plot(out_steady[,1],out_steady[,2], 
     type = "l",
     main = "Spiking membrane potential of neurons (fast spiking)",
     xlab = "time(ms)",
     ylab = "membrane potential(mV)")
lines(out_steady[,3], col = "red")
```
\newline 
Figuur 5: Neuronen zijn op sommige momenten in staat om zonder te verslomen series van spikes met een hele hoge frequentie te produceren.

\newpage
### Low-threshold Spiking Neurons
```{r model_LTS}
library(deSolve)
#define parameters
parameters_st <- c(a = 0.02, b = 0.25, c = -65, 
                   d = 2, I = 10)

#intial values
state <- c(v = -60,
           u = -20)

#time indication
times <- seq(0, 300, by = 1)

#spiking function
spiking <-function(t,y,parms){
    with(as.list(c(y,parms)),{
      dv <- (0.04*(v^2))+(5*v)+140-u+I
      du <- a*((b*v)-u)
        return(list(c(dv,du)))
    }
    )
}

root <- function(times,state,parms){
  #if v is above 30 root = False
  return(state[1]+30)
}
# if statment if v is above 30
event <- function(time,state,parms){ 
  with(as.list(c(state, parms)), { 
    #change intial values
    state[1] <- c 
    state[2] <- state[2] + d 
    return(state) 
  }) 
} 

#calculate deSolve graph matrix
out_steady <- lsode(times = times, y = state,
                  parms = parameters_st, 
                  func = spiking, 
                  events = list(func = event, 
                                root = TRUE),
                  rootfun = root
                  )

#plot low-threshold spiking (LTS)
plot(out_steady[,1],out_steady[,2], 
     type = "l",
     main = "Spiking membrane potential of neurons (low-threshold spiking)",
     xlab = "time(ms)",
     ylab = "membrane potential(mV)")
lines(out_steady[,3], col = "red")
```
Figuur 6: Neuronen zijn ook in staat in tegenstelling tot "fast spiking" om series van spikes voort te brengen waar een opvallende verandering in frequentie plaats vindt.
\newpage

### Linked Neurons
Met dit model is het mogelijk om meerdere neuronen tegelijk te laten simuleren.
Door twee soorten neuronen te laten simuleren is er te zien hoe de twee met elkaar reageren als ze synaptische connecties hebben(matrix "S"). In het onderzoek hebben we 1000 neuronen waarvan 800 "excitatory" en 200 "inhibitory" neuronen gesimuleerd. Deze 1000 neuronen zijn gelinkt met de al eerder genoemde matrix "S" waarin 1 000 000 synaptische connecties zijn gemaakt.
De parameters zijn per neuron random gekozen en worden per tijdeenheid veranderd in een array. De simulatie is dus array bound en kan moeilijk uitgevoerd worden met deSolve.

```{r linked_neurons}
#import library
library(ggplot2)

#make time indication
times <- seq(0, 300, by = 1)

#create random data linked to parameters of 800 excitatory 
#and 200 inhibitory neurons

#neuron numbers
Ne <- 800
Ni <- 200
tot <- Ne+Ni

#random factor
re <- replicate(1,runif(Ne,0,1))
ri <- replicate(1,runif(Ni,0,1))

#parameters a, b and c 
a <- array(rep(c(0.02,0.02+0.08),times = c(Ne,Ni)), c(1000,1))
a[(Ne+1):tot,] <- a[(Ne+1):tot,] *ri

b <- array(rep(c(0.2,0.25-0.05),times = c(Ne,Ni)), c(1000,1))
b[(Ne+1):tot,] <- b[(Ne+1):tot,] *ri

c <- array(rep(c(-65+15,-65), times = c(Ne,Ni)), c(1000,1))
c[0:Ne,] <- c[0:Ne,] *(re^2)

d <- array(rep(c(8-6,2), times = c(Ne,Ni)), c(1000,1))
d[0:Ne,] <- d[0:Ne,] *(re^2)

#S matrix for synaptic connection between neurons
S <- replicate(Ne,0.5*runif(tot,0,1))
S <- cbind(S, replicate(Ni,-runif(tot,0,1)))

#initial values
v <- array(rep(-65 , tot), c(1000,1))
u <- array(rep(1,tot), c(1000,1))

for(i in times){
    u[i,] <- v[i,] * b[i,]
}
firings <- array()

#run neuron simulation for every ms(t) in the time indication
for(t in times){
    fired <- array()
    #create I
    I1 <- replicate(1,5*rnorm(Ne))
    I2 <- replicate(1,2*rnorm(Ni))
    I  <- rbind(I1,I2)
    #for every v above 30 save index
    fired <- which(v >= 100)
    for(i in fired){
        #save every v above 30 with its timepoint
        firings <- rbind(firings,c(t,v[i,]))
        #change initial values
        v[i,] <- c[i,]
        u[i,] <- u[i,]+d[i,]
        I[i,] <- I[i,]+sum(S[i,])
    }
    
    #change initial values with formula
    v <- v+(0.04*(v^2))+(5*v)+140-u+I
    u <- u+a*(b*v-u)
}

#remove first line of "NA's" in firings
firings <- firings[-1, ]
#make horizontal line that divides inhibitory with excitatory neurons
cutoff <- data.frame( x = c(-Inf, Inf), y = 800, cutoff = factor(800) )
#plot firings
ggplot(as.data.frame(firings), aes(x = firings[,1],y = firings[,2])) +
   geom_point(shape = 16, size = 0.07) +
  theme_minimal()+
  labs(x = "time, ms", 
       y = "neuron number", 
       title = "simulation of 1000 coupled spiking neurons") +
  geom_line(aes( x, y, linetype = cutoff ), cutoff)
```
Figuur 7: Door ons gecreërde simulatie van 1000 random neuronen. Meer dan 300 ms was niet mogelijk om te simuleren. Boven de cutoff; inhibitory, onder de cutoff; excitatory.

![Visuele ondersteuning voor het model. Neuron en een neuron spike](linked.png)
\newline
Figuur 8: Door M. Izhikevich gecreërd model van linked neuronen d.m.v. MATLAB.

\newpage

## Conclusie en Discussie
In dit artikel is er een poging gedaan om het werk van M. Izhikevich na te bootsen. Dit houdt in dat er geprobeerd is een
simpel model te realiseren die het veelzijdige gedrag van neuronen weergeeft. Het model wat gebruikt is bestaat slechts uit twee formules
wat in tegenstelling tot andere, meer gecompliceerde modellen, weinig is.
In dit artikel zijn de volgende typen "spiking neurons" succesvol gemodelleerd; "Regular Spiking Neurons", "Intrinsically Bursting Neurons", 
"Chattering Neurons", "Fast Spiking Neurons" en "Low-threshold Spiking Neurons". \newline
Daarnaast is ook geprobeerd twee typen neuronen te linken. Dit was echter lastig te reproduceren aangezien in het artikel van
M. Izhikevich hiervoor MATLAB code werd gebruikt. Hierdoor was het niet mogelijk om dit met het deSolve pakket uit te voeren.
Toch is door middel van reguliere R-code een model gecreërd. \newline
MATLAB is een uiterst geschikte taal voor het verwerken van grote berekeningen over bijvoorbeeld een array. Aangezien het voor R te veel data
is had R hier dan ook moeite mee. R heeft namelijk een limiet voor de lengte van arrays. Ook duurt een berekening als in het code blok "linked_neurons" op een losstaande PC ongeveer al twintig minuten. Het had niet echt een meerwaarde om de array limiet te verhogen aangezien het dan nog langer zou duren en geen significante toevoeging had aan het resultaat, vandaar is er maar 300ms te zien in plaats van 1000ms.
De verdeling van het werk is niet heel slim aangepakt. Het was goed verdeeld, maar op een gegeven moment wist ik weinig van het geen wat mijn partner deed, en hij wist weinig van wat ik deed. Achteraf was het misschien beter om alles samen te doen.
\newpage

## Referenties

[1] RStudio: Open source and enterprise-ready professional software for R \newline
    https://www.rstudio.com/ \newline
[2] deSolve: Solvers for Initial Value Problems of Differential Equations ('ODE', 'DAE', 'DDE')
    Author: Karline Soetaert [aut], Thomas Petzoldt [aut, cre], R. Woodrow Setzer [aut], odepack authors [cph] \newline
    https://cran.r-project.org/web/packages/deSolve/index.html \newline
[3] Simple Model of Spiking Neurons by Eugene M. Izhikevich \newline
    https://www.izhikevich.org/publications/spikes.pdf
[4] Function of Nerves - Action of Anesthetics  \newline
    http://www.gamma.nbi.dk/Galleri/gamma143/nerves.pdf  \newline
[5] Solitons in cell membranes  \newline
    http://link.aps.org/abstract/PRE/v51/p3588

