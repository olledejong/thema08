---
title: "Neuron spiking model"
author: "Menno Gerbens & Olle de Jong"
date: "31-5-2018"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(cache = TRUE)
```
Met het onderstaande model wordt de "neuron spiking" gemodelleerd. Het model bestaat uit twee formules met 5 parameters(a,b,c,d en I). Voor de rest bestaat het model uit twee intiële waardes(v en u). Door het veranderen van de parameters kunnen gevarieerde soorten spike grafieken gemodelleerd worden die verbonden zijn aan verschillende soorten neuronen op verschillende plekken in de hersens.

```{r model}
#import library
library(deSolve)

#define parameters
parameters_st <- c(a = 0.02, b = 0.2, c = -50,
                  d =2, I = 10)
#intial values
state <- c(v = -60,
           u = -10)

#time indication
times <- seq(0, 300, by = 1)

#spiking function
spiking <-function(t,y,parms){
    with(as.list(c(y,parms)),{
      #formula's
      dv <- (0.04*(v^2))+(5*v)+140-u+I
      du <- a*((b*v)-u)
        return(list(c(dv,du)))
    }
    )
}
#root function
root <- function(times,state,parms){
  #if v is above 30 root = False
  return(state[1]+30)
}
# if statment if v is above 30
event <- function(time,state,parms){ 
  with(as.list(c(state, parms)), { 
    #change intial values
    state[1] <- c 
    state[2] <- state[2] + d 
    return(state) 
  }) 
} 

#calculate deSolve graph matrix
out_steady <- lsode(times = times, y = state,
                  parms = parameters_st, 
                  func = spiking, 
                  events = list(func = event, 
                                root = TRUE),
                  rootfun = root
                  )
#plot deSolve results
plot(out_steady[,1],out_steady[,2], 
     type = "l",
     main = "Spiking membrane potential of neurons",
     xlab = "time(ms)",
     ylab = "membrane potential(mV)")
lines(out_steady[,3], col = "red")

```

```{r linked_neurons}
#import library
library(ggplot2)

#make time indication
times <- seq(0, 300, by = 1)

#create random data linked to parameters of 800 excitatory 
#and 200 inhibitory neurons

#neuron numbers
Ne <- 800
Ni <- 200
tot <- Ne+Ni

#random factor
re <- replicate(1,runif(Ne,0,1))
ri <- replicate(1,runif(Ni,0,1))

#parameters a, b and c 
a <- array(rep(c(0.02,0.02+0.08),times = c(Ne,Ni)), c(1000,1))
a[(Ne+1):tot,] <- a[(Ne+1):tot,] *ri

b <- array(rep(c(0.2,0.25-0.05),times = c(Ne,Ni)), c(1000,1))
b[(Ne+1):tot,] <- b[(Ne+1):tot,] *ri

c <- array(rep(c(-65+15,-65), times = c(Ne,Ni)), c(1000,1))
c[0:Ne,] <- c[0:Ne,] *(re^2)

d <- array(rep(c(8-6,2), times = c(Ne,Ni)), c(1000,1))
d[0:Ne,] <- d[0:Ne,] *(re^2)

#S matrix for synaptic connection between neurons
S <- replicate(Ne,0.5*runif(tot,0,1))
S <- cbind(S, replicate(Ni,-runif(tot,0,1)))

#initial values
v <- array(rep(-65 , tot), c(1000,1))
u <- array(rep(1,tot), c(1000,1))

for(i in times){
    u[i,] <- v[i,] * b[i,]
}
firings <- array()

#run neuron simulation for every ms(t) in the time indication
for(t in times){
    fired <- array()
    #create I
    I1 <- replicate(1,5*rnorm(Ne))
    I2 <- replicate(1,2*rnorm(Ni))
    I  <- rbind(I1,I2)
    #for every v above 30 save index
    fired <- which(v >= 100)
    for(i in fired){
        #save every v above 30 with its timepoint
        firings <- rbind(firings,c(t,v[i,]))
        #change initial values
        v[i,] <- c[i,]
        u[i,] <- u[i,]+d[i,]
        I[i,] <- I[i,]+sum(S[i,])
    }
    
    #change initial values with formula
    v <- v+(0.04*(v^2))+(5*v)+140-u+I
    u <- u+a*(b*v-u)
}
#plot neurons
ggplot(as.data.frame(firings), aes(x = firings[,1],y = firings[,2])) +
   geom_point(shape = 16, size = 0.07) +
  theme_minimal()+
  labs(x = "time, ms", 
       y = "neuron number", 
       title = "simulation of 1000 coupled spiking neurons")
```